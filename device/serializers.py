from rest_framework import serializers

class CameraSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(allow_blank=False, max_length=100)
    type = serializers.CharField(allow_blank=False)
    ip = serializers.CharField(allow_blank=False)
    status = serializers.BooleanField()


class BoxSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(allow_blank=False, max_length=100)
    ip = serializers.CharField(allow_blank=False)
    account = serializers.CharField(allow_blank=False)
    password = serializers.CharField(allow_blank=False)
    status = serializers.BooleanField()