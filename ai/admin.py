from django.contrib import admin

# Register your models here.
from .models import Algorithm


admin.site.register(Algorithm)