from django.db import models

# Create your models here.

class Camera(models.Model):
    name = models.CharField(max_length=50)
    type= models.CharField(max_length=50)
    ip = models.CharField(max_length=50)
    status = models.BooleanField()

    def __str__(self):
        return self.name
    


class Box(models.Model):
    name = models.CharField(max_length=50)
    topic = models.CharField(max_length=50)
    status = models.BooleanField()

    def __str__(self):
        return self.name