from django.db import models

# Create your models here.
class Picture(models.Model):
    name = models.CharField(max_length=50)
    path = models.CharField(max_length=100)

    def __str__(self):
        return self.name