from rest_framework import serializers

class AlgorithmSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(allow_blank=False, max_length=100)
    path = serializers.CharField(allow_blank=False)
