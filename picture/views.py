from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from .models import Picture
from .serializers import PictureSerializer
# Create your views here.

# 显示所有图片
@api_view(['GET'])
def picture_list(request):
    if request == 'GET':
        pictures = Picture.objects.all()
        serializer = PictureSerializer(pictures, many=True)
        return Response(serializer.data)


# 单个图片
@api_view(['GET'])
def picture_detail(request, id):
    if request == 'GET':
        picture = Picture.objects.get(id=id)
        serializer = PictureSerializer(picture, many=True)
        return Response(serializer.data)