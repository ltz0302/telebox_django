from django.urls import path
from . import views

app_name = 'device'

urlpatterns = [
     path('camera-list/', views.camera_list, name='camera_list'),
     path('box-list/', views.box_list, name='box_list'),
]