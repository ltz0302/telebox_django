from django.urls import path
from . import views

app_name = 'video'

urlpatterns = [
    path('video-list/', views.video_list, name='video_list'),
    path('video-detail/', views.video_detail, name='video_detail'),
]