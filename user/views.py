from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from .serializers import UserSerializer
from django.contrib.auth.models import User

# Create your views here.
@api_view(['POST'])
def user_login(request):
    if request.method == 'POST':
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            # 可以在这里访问验证通过后的数据
            username = serializer.validated_data['username']
            password = serializer.validated_data['password']
            user = authenticate(username=username, password=password)
            if user:
                # 将用户数据保存在 session 中，即实现了登录动作
                login(request, user)
                response_data = {
                    'username': serializer.data.get('username', None),
                    'message': '登录成功'
                }
                return Response(response_data, status=status.HTTP_200_OK)
            else:
                return Response({'message': '账号或密码输入有误。请重新输入'})
        else:
            return Response({'message': '账号或密码输入不合法'})
    else:
        return Response({'message': '请使用GET或POST请求数据'})
    


@api_view(['GET'])
@login_required(login_url='/user/user-login/')
def user_logout(request):
    if request.method == 'GET':
        logout(request)
        return Response({'message': '退出成功'}, status=status.HTTP_200_OK)



@api_view(['GET'])
def user_register(request):
    if request.method == 'GET':
        password = '123456'
        new_user = User(username='zxc', password=password)
        new_user.set_password(password)
        new_user.save()
        return Response({'message': '注册成功'}, status=status.HTTP_200_OK)