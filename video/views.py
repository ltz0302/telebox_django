from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from .models import Video
from .serializers import VideoSerializer


# Create your views here.


# 显示所有视频
@api_view(['GET'])
def video_list(request):
    if request == 'GET':
        videos = Video.objects.all()
        serializer = VideoSerializer(videos, many=True)
        return Response(serializer.data)


# 详情页单个视频
@api_view(['GET'])
def video_detail(request, id):
    if request == 'GET':
        video = Video.objects.get(id=id)
        serializer = VideoSerializer(video, many=True)
        return Response(serializer.data)