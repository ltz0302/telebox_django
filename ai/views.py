from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from .models import Algorithm
from device.models import Camera
from .serializers import AlgorithmSerializer
from mqtt.views import publish_mqtt_message

# Create your views here.


# 显示所有可用算法
@api_view(['GET'])
def algorithm_list(request):
    if request.method == 'GET':
        algorithms = Algorithm.objects.all()
        serializer = AlgorithmSerializer(algorithms, many=True)
        return Response(serializer.data)



@api_view(['GET'])
def algorithm_apply(request, camera_id, algorithm_id):
    if request.method == 'GET':
        # box = Box.objects.get(id=box_id) 
        # topic = box.topic
        algorithm = Algorithm.objects.get(id=algorithm_id)
        path = algorithm.path
        command = "sh" + path
        mqtt_send(camera_id, command)
        # send_to_kafka(topic=topic, message=command)
        return Response({'message': '执行成功'}, status=status.HTTP_200_OK)




# 新增一个算法
def algorithm_add(request):
    if request.method == 'GET':
        name = '抽烟检测'
        path = '/tmp/smoke.py'
        algorithm = Algorithm(name, path) 
        algorithm.save()
        return Response({'message': '添加成功'}, status=status.HTTP_200_OK)
    



# 删除所选算法
@api_view(['GET'])
def algorithm_delete(request, id):
    if request.method == 'GET':
        algorithm = Algorithm.objects.get(id=id)
        algorithm.delete()
        return Response({'message': '删除成功'}, status=status.HTTP_200_OK)
    



def mqtt_send(camera_id, message):
    topic = 'telebox' + str(camera_id)
    publish_mqtt_message(topic, message)
    print(message)
    return Response({'message': '发送成功'}, status=status.HTTP_200_OK)



@api_view(['GET'])
def mqtt_test(request):
    topic = 'telebox'
    message = 'ls -l'
    publish_mqtt_message(topic, message)
    return Response({'message': '发送成功'}, status=status.HTTP_200_OK)