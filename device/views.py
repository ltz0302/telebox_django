from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from .models import Camera, Box
from .serializers import CameraSerializer, BoxSerializer
# Create your views here.


@api_view(['GET'])
def camera_list(request):
    if request == 'GET':
        cameras = Camera.objects.all()
        serializer = CameraSerializer(cameras, many=True)
        return Response(serializer.data)
    

@api_view(['GET'])
def box_list(request):
    if request == 'GET':
        boxes = Box.objects.all()
        serializer = BoxSerializer(boxes, many=True)
        return Response(serializer.data)



@api_view(['GET'])
def box_add(request):
    if request == 'GET':
        name = 'box1'
        topic = 'telebox'
        status = True
        box = Box(name, topic, status)
        box.save()
        return Response({'message': '添加成功'}, status=status.HTTP_200_OK)