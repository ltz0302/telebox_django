from rest_framework import serializers

class VideoSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(allow_blank=False, max_length=100)
    url = serializers.CharField(allow_blank=False)