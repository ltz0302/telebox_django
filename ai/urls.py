from django.urls import path
from . import views

app_name = 'ai'

urlpatterns = [
    path('algorithm-list/', views.algorithm_list, name='algorithm_list'),
    path('algorithm-apply/<int:camera_id>/<int:algorithm_id>/', views.algorithm_apply, name='algorithm_apply'),
    path('algorithm-delete/<int:id>/', views.algorithm_delete, name='algorithm_delete'),
    path('mqtt-test/', views.mqtt_test, name='mqtt_test'),
]