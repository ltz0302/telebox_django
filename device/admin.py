from django.contrib import admin

# Register your models here.
from .models import Camera,Box


admin.site.register(Camera)
admin.site.register(Box)